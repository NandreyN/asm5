/*
1) ������ ��������, ������������ �������� �������� ��������� -
������� �� ��������, � ������ �������� � �������
*/
#undef UNICODE
#include <windows.h>
#include  <math.h>
#include <vector>
#include <string>
#include <algorithm>
#include <stack>

using namespace std;
int _arrayLength = 10;

extern "C" int _cdecl getMiddleElementIndex(const int*, int);
string ConvertArrayToString(const int * arr, int len);
string ExecuteTest(const vector<int>& vect);
BOOL InitWnd(HINSTANCE hinstance);
BOOL InitInstance(HINSTANCE hinstance, int nCmdShow);
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam);
int WINAPI WinMain(HINSTANCE hinstance, HINSTANCE prevHinstance, LPSTR lpCmdLine, int nCmdShow)
{
	MSG msg;

	if (!InitWnd(hinstance))
	{
		MessageBox(NULL, "Unable to Init App", "Error", MB_OK);
		return FALSE;
	}

	if (!InitInstance(hinstance, nCmdShow))
	{
		MessageBox(NULL, "Unable to Init Instance", "Error", MB_OK);
		return FALSE;
	}

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

BOOL InitWnd(HINSTANCE hinstance)
{
	WNDCLASSEX wndclass;
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc = WndProc;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hinstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_CROSS);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = "ASM";
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	if (!RegisterClassEx(&wndclass))
	{
		MessageBox(NULL, "Cannot register class", "Error", MB_OK);
		return FALSE;
	}
	return TRUE;
}
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
	static int x, y, line;
	static vector<HWND> fields;
	static string textOut;
	static HWND applyButton;
	static double cellWidth, interval;
	static HDC hdc;
	PAINTSTRUCT ps;
	static RECT r;
	static vector<int> a;
	switch (message)
	{
	case WM_CREATE:
		GetClientRect(hwnd, &r);
		x = r.right; y = r.bottom;
		

		a = {1,4,5,3,2};
		textOut += ExecuteTest(a);
		a = {1,2,3,43545,55};
		textOut += ExecuteTest(a);
		a = { 11,22,78,96,3,0,0,0,1,4,7};
		textOut += ExecuteTest(a);
		a = { 1 };
		textOut += ExecuteTest(a);
		a = {4,8,7,4,3,4,4,0};
		textOut += ExecuteTest(a);
		a = { 2,2 };
		textOut += ExecuteTest(a);
		MessageBox(NULL, textOut.data(), "", MB_OK);
		SendMessage(hwnd, WM_CLOSE, NULL, NULL);
		break;

	case WM_SIZE:
		GetClientRect(hwnd, &r);
		x = r.right; y = r.bottom;


		break;
	case WM_COMMAND:
		
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hwnd, message, wparam, lparam);
	}
	return FALSE;
}

BOOL InitInstance(HINSTANCE hinstance, int nCmdShow)
{
	HWND hwnd;
	hwnd = CreateWindow(
		"ASM",
		"ASM",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		0,
		CW_USEDEFAULT,
		0,
		NULL,
		NULL,
		hinstance,
		NULL);


	if (!hwnd)
		return FALSE;
	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd);
	return TRUE;
}
string ConvertArrayToString(const int * arr, int len)
{
	string toReturn = "\"";
	for (int i = 0; i < len; i++)
		toReturn += to_string(arr[i]) + ",";
	toReturn += "\"";
	return toReturn;
}
string ExecuteTest(const vector<int>& vect)
{
	string ret;
	int b = getMiddleElementIndex(vect.data(), vect.size());
	ret += "Test: " + ConvertArrayToString(vect.data(), vect.size()) + ", index = " + to_string(b) + "\n";
	return ret;
}