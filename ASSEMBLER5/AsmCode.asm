;;1) ������ ��������, ������������ �������� �������� ��������� -
;;������� �� ��������, � ������ �������� � �������

.586
.MODEL FLAT
.DATA
less DD 0
more DD 0
prevDiff DD 0
retVal DD 0
arrLen DD 0
.CODE
PUBLIC _getMiddleElementIndex

abs PROC
mov eax , [esp + 4]
cmp eax , 0
jge ok
neg eax
ok:ret
abs ENDP

_getMiddleElementIndex PROC
push ebp
push edi
mov ebp, esp
mov edi , [ebp + 12]
mov ebx, [ebp + 16]
mov prevDiff , ebx
mov arrLen , ebx
;; enumerate

mov ecx , ebx
xor edx, edx; curretn index
enumeration:
push ecx
mov ebx, [edi + edx*4]; current element
xor esi ,esi; new index
mov less, 0
mov more , 0
mov ecx,arrLen 
iter:
cmp edx,esi
je skip_element
cmp ebx , [edi + esi * 4]
jl ls
mr:
inc more
jmp skip_element
ls:
inc less

skip_element:
inc esi
loop iter

; check
mov ebx , less
sub ebx , more
;; get abs 
push ebx
call abs
pop ebx
; result in eax
cmp eax, prevDiff
jge next_enum
mov prevDiff, eax
mov retVal, edx

next_enum:
pop ecx
inc edx
loop enumeration

brk:
mov eax , retVal

pop edi
pop ebp
ret
_getMiddleElementIndex ENDP


END